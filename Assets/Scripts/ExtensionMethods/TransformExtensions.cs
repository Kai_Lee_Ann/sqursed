﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ExtensionMethods
{
    public static class TransformExtensions
    {
        public static IEnumerable<T> GetComponentsInChildrenOfType<T>(this Transform transform)
        {
            return transform.GetComponentsInChildren<MonoBehaviour>()
                .OfType<T>()
                .ToList();
        }
    }
}