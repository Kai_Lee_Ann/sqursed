﻿namespace Core
{
    public static class Tags
    {
        public const string Player = "Player";
        public const string Obstacle = "Obstacle";
        public const string FinishZone = "Finish";
        public const string TurnZone = "Turn";
    }
}