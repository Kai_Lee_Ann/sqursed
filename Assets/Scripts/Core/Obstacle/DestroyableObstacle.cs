﻿using UnityEngine;

namespace Core.Obstacle
{
    public class DestroyableObstacle : MonoBehaviour
    {
        private void OnCollisionEnter(Collision other)
        {
            if (!other.transform.CompareTag(Tags.Player))
            {
                return;
            }
            Destroy(gameObject);
        }
    }
}