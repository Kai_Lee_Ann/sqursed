﻿using Core.Service.Broadcast;
using TMPro;
using UnityEngine;

namespace Core.Service.Score
{
    public class ScoreLabelUpdater : MonoBehaviour
    {
        private TextMeshProUGUI _label;

        private void Awake()
        {
            _label = GetComponent<TextMeshProUGUI>();
            Messenger.AddListener<int>(Events.ScoreUpdated, UpdateScoreLabel);
        }
        
        private void UpdateScoreLabel(int score)
        {
            _label.text = $"Score: {score}";
        }
    }
}