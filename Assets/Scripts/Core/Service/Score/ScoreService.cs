﻿using Core.Service.Broadcast;
using UnityEngine;

namespace Core.Service.Score
{
    public class ScoreService : MonoBehaviour
    {
        private int _score;

        private void Awake()
        {
            Messenger.AddListener(Events.CollectablePickedUp, UpdateScore);
        }

        private void Start()
        {
            Messenger.Broadcast(Events.ScoreUpdated, _score);
        }

        private void UpdateScore()
        {
            _score++;
            Messenger.Broadcast(Events.ScoreUpdated, _score);
        }
    }
}