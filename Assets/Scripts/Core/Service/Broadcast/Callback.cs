﻿namespace Core.Service.Broadcast
{
    public delegate void Callback();
    public delegate void Callback<in T>(T arg1);
    public delegate void Callback<in T, in TU>(T arg1, TU arg2);
    public delegate void Callback<in T, in TU, in TV>(T arg1, TU arg2, TV arg3);
}