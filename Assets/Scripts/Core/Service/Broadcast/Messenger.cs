﻿/*
* Advanced C# messenger by Ilya Suzdalnitski. V1.0
* 
* Based on Rod Hyde's "CSharpMessenger" and Magnus Wolffelt's "CSharpMessenger Extended".
* 
* Features:
  * Prevents a MissingReferenceException because of a reference to a destroyed message handler.
  * Option to log all messages
  * Extensive error detection, preventing silent bugs
* 
* Usage examples:
  1. Messenger.AddListener<GameObject>("prop collected", PropCollected);
     Messenger.Broadcast<GameObject>("prop collected", prop);
  2. Messenger.AddListener<float>("speed changed", SpeedChanged);
     Messenger.Broadcast<float>("speed changed", 0.5f);
* 
* Messenger cleans up its evenTable automatically upon loading of a new level.
* 
* Don't forget that the messages that should survive the cleanup, should be marked with Messenger.MarkAsPermanent(string)
* 
*/

// Slightly modified for modern C#

//#define LOG_ALL_MESSAGES
//#define LOG_ADD_LISTENER
//#define LOG_BROADCAST_MESSAGE

#define REQUIRE_LISTENER

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Service.Broadcast
{
    internal static class Messenger
    {
        #region Internal variables

        //Disable the unused variable warning
#pragma warning disable 0414
        //Ensures that the MessengerHelper will be created automatically upon start of the game.
        private static MessengerHelper _messengerHelper =
            new GameObject("MessengerHelper").AddComponent<MessengerHelper>();
#pragma warning restore 0414

        private static readonly Dictionary<string, Delegate> EventTable = new Dictionary<string, Delegate>();

        //Message handlers that should never be removed, regardless of calling Cleanup
        private static readonly List<string> PermanentMessages = new List<string>();

        #endregion

        #region Helper methods

        //Marks a certain message as permanent.
        public static void MarkAsPermanent(string eventType)
        {
#if LOG_ALL_MESSAGES
		Debug.Log("Messenger MarkAsPermanent \t\"" + eventType + "\"");
#endif

            PermanentMessages.Add(eventType);
        }


        public static void Cleanup()
        {
#if LOG_ALL_MESSAGES
		Debug.Log("MESSENGER Cleanup. Make sure that none of necessary listeners are removed.");
#endif

            var messagesToRemove = (
                from pair in EventTable
                where PermanentMessages.All(
                    message => pair.Key != message
                )
                select pair.Key
            ).ToList();

            foreach (var message in messagesToRemove)
            {
                EventTable.Remove(message);
            }
        }

        public static void PrintEventTable()
        {
            Debug.Log("\t\t\t=== MESSENGER PrintEventTable ===");

            foreach (var pair in EventTable)
            {
                Debug.Log("\t\t\t" + pair.Key + "\t\t" + pair.Value);
            }

            Debug.Log("\n");
        }

        #endregion

        #region Message logging and exception throwing

        private static void OnListenerAdding(string eventType, Delegate listenerBeingAdded)
        {
#if LOG_ALL_MESSAGES || LOG_ADD_LISTENER
		Debug.Log("MESSENGER OnListenerAdding \t\"" + eventType + "\"\t{" + listenerBeingAdded.Target + " -> " + listenerBeingAdded.Method + "}");
#endif

            if (!EventTable.ContainsKey(eventType))
            {
                EventTable.Add(eventType, null);
            }

            var d = EventTable[eventType];
            if (d != null && d.GetType() != listenerBeingAdded.GetType())
            {
                throw new ListenerException(
                    $"Attempting to add listener with inconsistent signature for event type {eventType}. Current listeners have type {d.GetType().Name} and listener being added has type {listenerBeingAdded.GetType().Name}"
                );
            }
        }

        private static void OnListenerRemoving(string eventType, Delegate listenerBeingRemoved)
        {
#if LOG_ALL_MESSAGES
		Debug.Log("MESSENGER OnListenerRemoving \t\"" + eventType + "\"\t{" + listenerBeingRemoved.Target + " -> " + listenerBeingRemoved.Method + "}");
#endif

            if (EventTable.ContainsKey(eventType))
            {
                var d = EventTable[eventType];

                if (d == null)
                {
                    throw new ListenerException(
                        $"Attempting to remove listener with for event type \"{eventType}\" but current listener is null."
                    );
                }

                if (d.GetType() != listenerBeingRemoved.GetType())
                {
                    throw new ListenerException(
                        $"Attempting to remove listener with inconsistent signature for event type {eventType}. Current listeners have type {d.GetType().Name} and listener being removed has type {listenerBeingRemoved.GetType().Name}"
                    );
                }
            }
            else
            {
                throw new ListenerException(
                    $"Attempting to remove listener for type \"{eventType}\" but Messenger doesn't know about this event type."
                );
            }
        }

        private static void OnListenerRemoved(string eventType)
        {
            if (EventTable[eventType] == null)
            {
                EventTable.Remove(eventType);
            }
        }

        private static void OnBroadcasting(string eventType)
        {
#if REQUIRE_LISTENER
            if (!EventTable.ContainsKey(eventType))
            {
                throw new BroadcastException(
                    $"Broadcasting message \"{eventType}\" but no listener found. Try marking the message with Messenger.MarkAsPermanent."
                );
            }
#endif
        }

        private static BroadcastException CreateBroadcastSignatureException(string eventType)
        {
            return new BroadcastException(
                $"Broadcasting message \"{eventType}\" but listeners have a different signature than the broadcaster."
            );
        }

        private class BroadcastException : Exception
        {
            public BroadcastException(string msg)
                : base(msg)
            {
            }
        }

        private class ListenerException : Exception
        {
            public ListenerException(string msg)
                : base(msg)
            {
            }
        }

        #endregion

        #region AddListener

        //No parameters
        public static void AddListener(string eventType, Callback handler)
        {
            OnListenerAdding(eventType, handler);
            EventTable[eventType] = (Callback) EventTable[eventType] + handler;
        }

        //Single parameter
        public static void AddListener<T>(string eventType, Callback<T> handler)
        {
            OnListenerAdding(eventType, handler);
            EventTable[eventType] = (Callback<T>) EventTable[eventType] + handler;
        }

        //Two parameters
        public static void AddListener<T, TU>(string eventType, Callback<T, TU> handler)
        {
            OnListenerAdding(eventType, handler);
            EventTable[eventType] = (Callback<T, TU>) EventTable[eventType] + handler;
        }

        //Three parameters
        public static void AddListener<T, TU, TV>(string eventType, Callback<T, TU, TV> handler)
        {
            OnListenerAdding(eventType, handler);
            EventTable[eventType] = (Callback<T, TU, TV>) EventTable[eventType] + handler;
        }

        #endregion

        #region RemoveListener

        //No parameters
        public static void RemoveListener(string eventType, Callback handler)
        {
            OnListenerRemoving(eventType, handler);
            EventTable[eventType] = (EventTable[eventType] as Callback) - handler;
            OnListenerRemoved(eventType);
        }

        //Single parameter
        public static void RemoveListener<T>(string eventType, Callback<T> handler)
        {
            OnListenerRemoving(eventType, handler);
            EventTable[eventType] = (EventTable[eventType] as Callback<T>) - handler;
            OnListenerRemoved(eventType);
        }

        //Two parameters
        public static void RemoveListener<T, TU>(string eventType, Callback<T, TU> handler)
        {
            OnListenerRemoving(eventType, handler);
            EventTable[eventType] = (EventTable[eventType] as Callback<T, TU>) - handler;
            OnListenerRemoved(eventType);
        }

        //Three parameters
        public static void RemoveListener<T, TU, TV>(string eventType, Callback<T, TU, TV> handler)
        {
            OnListenerRemoving(eventType, handler);
            EventTable[eventType] = (EventTable[eventType] as Callback<T, TU, TV>) - handler;
            OnListenerRemoved(eventType);
        }

        #endregion

        #region Broadcast

        //No parameters
        public static void Broadcast(string eventType)
        {
#if LOG_ALL_MESSAGES || LOG_BROADCAST_MESSAGE
		Debug.Log("MESSENGER\t" + System.DateTime.Now.ToString("hh:mm:ss.fff") + "\t\t\tInvoking \t\"" + eventType + "\"");
#endif
            OnBroadcasting(eventType);

            if (!EventTable.TryGetValue(eventType, out var d))
            {
                return;
            }

            if (d is Callback callback)
            {
                callback();
            }
            else
            {
                throw CreateBroadcastSignatureException(eventType);
            }
        }

        //Single parameter
        public static void Broadcast<T>(string eventType, T arg1)
        {
#if LOG_ALL_MESSAGES || LOG_BROADCAST_MESSAGE
		Debug.Log("MESSENGER\t" + System.DateTime.Now.ToString("hh:mm:ss.fff") + "\t\t\tInvoking \t\"" + eventType + "\"");
#endif
            OnBroadcasting(eventType);

            if (!EventTable.TryGetValue(eventType, out var d))
            {
                return;
            }

            if (d is Callback<T> callback)
            {
                callback(arg1);
            }
            else
            {
                throw CreateBroadcastSignatureException(eventType);
            }
        }

        //Two parameters
        public static void Broadcast<T, TU>(string eventType, T arg1, TU arg2)
        {
#if LOG_ALL_MESSAGES || LOG_BROADCAST_MESSAGE
		Debug.Log("MESSENGER\t" + System.DateTime.Now.ToString("hh:mm:ss.fff") + "\t\t\tInvoking \t\"" + eventType + "\"");
#endif
            OnBroadcasting(eventType);

            if (!EventTable.TryGetValue(eventType, out var d))
            {
                return;
            }

            if (d is Callback<T, TU> callback)
            {
                callback(arg1, arg2);
            }
            else
            {
                throw CreateBroadcastSignatureException(eventType);
            }
        }

        //Three parameters
        public static void Broadcast<T, TU, TV>(string eventType, T arg1, TU arg2, TV arg3)
        {
#if LOG_ALL_MESSAGES || LOG_BROADCAST_MESSAGE
		Debug.Log("MESSENGER\t" + System.DateTime.Now.ToString("hh:mm:ss.fff") + "\t\t\tInvoking \t\"" + eventType + "\"");
#endif
            OnBroadcasting(eventType);

            if (!EventTable.TryGetValue(eventType, out var d))
            {
                return;
            }

            if (d is Callback<T, TU, TV> callback)
            {
                callback(arg1, arg2, arg3);
            }
            else
            {
                throw CreateBroadcastSignatureException(eventType);
            }
        }

        #endregion
    }

//This manager will ensure that the messenger's eventTable will be cleaned up upon loading of a new level.
    public sealed class MessengerHelper : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            SceneManager.sceneUnloaded += Cleanup;
        }

        private static void Cleanup(Scene scene)
        {
            Messenger.Cleanup();
        }
    }
}