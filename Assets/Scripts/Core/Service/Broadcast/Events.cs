﻿namespace Core.Service.Broadcast
{
    public static class Events
    {
        public const string CollectablePickedUp = "CollectablePickedUp";
        public const string ScoreUpdated = "ScoreUpdated";
    }
}