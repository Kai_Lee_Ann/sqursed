﻿using Core.Service.Broadcast;
using UnityEngine;

namespace Core.Collectable
{
    public class Collectable : MonoBehaviour
    {
        [Header("Rotation")]
        [SerializeField] private float degreesPerSecond;

        [Header("Floating")] 
        [SerializeField] private float amplitude;
        [SerializeField] private float frequency;

        private Vector3 _posOffset;

        private void Awake()
        {
            _posOffset = transform.position;
        }

        private void OnCollisionEnter(Collision other)
        {
            if (!other.gameObject.CompareTag(Tags.Player))
            {
                return;
            }

            Messenger.Broadcast(Events.CollectablePickedUp);
            Destroy(gameObject);
        }

        private void Update()
        {
            transform.Rotate(new Vector3(0f, Time.deltaTime * degreesPerSecond, 0f), Space.World);
            var tempPos = _posOffset;
            tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;
            transform.position = tempPos;
        }
    }
}