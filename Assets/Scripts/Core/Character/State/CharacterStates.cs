﻿using System;

namespace Core.Character.State
{
    public static class CharacterStates
    {
        public static readonly Type StartingCharacterState = typeof(StartingCharacterState);
        public static readonly Type MoveForwardCharacterState = typeof(MoveForwardCharacterState);
        public static readonly Type RecoilCharacterState = typeof(RecoilCharacterState);
        public static readonly Type FinishCharacterState = typeof(FinishCharacterState);
        public static readonly Type TurnCharacterState = typeof(TurnCharacterState);
    }
}