﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Character.State
{
    public class FinishCharacterState : AbstractCharacterState
    {
        private void Update()
        {
            if (Input.anyKey)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }
}