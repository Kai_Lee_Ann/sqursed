﻿using System.Collections;
using UnityEngine;

namespace Core.Character.State
{
    public class RecoilCharacterState : AbstractCharacterState
    {
        [SerializeField] private float recoilTime;
        [SerializeField] private float recoilSpeed;

        private void OnEnable()
        {
            StartCoroutine(Recoil());
        }

        private void Update()
        {
            transform.Translate(recoilSpeed * Time.deltaTime * Vector3.back);
        }

        private IEnumerator Recoil()
        {
            // Debug.Log("Recoil started ...");
            yield return new WaitForSeconds(recoilTime);
            // Debug.Log("Recoil finished");
            NextState = CharacterStates.MoveForwardCharacterState;
        }
    }
}