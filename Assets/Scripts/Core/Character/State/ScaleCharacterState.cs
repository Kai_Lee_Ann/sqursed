﻿using UnityEngine;

namespace Core.Character.State
{
    public class ScaleCharacterState : AbstractCharacterState
    {
        [Header("Projection")]
        public GameObject projection;
        
        [Header("Scaling Settings")]
        public float minHeight;
        public float maxHeight;
        public float scalingFactor;
        public float minScaleThreshold;

        private Vector3 _mousePosition;
        private Vector3 _dragStartPosition;
        private Vector3 _dragPath;

        private void OnEnable()
        {
            projection.SetActive(true);
        }
        
        public void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _dragStartPosition = Input.mousePosition;
                // Debug.Log($"Drag start set to {_dragStartPosition}");
            }
            if (!Input.GetMouseButton(0))
            {
                return;
            }
            if (IsDirectionChanged(_dragPath, Input.mousePosition - _mousePosition))
            {
                // Debug.Log($"Drag start changed from {_dragStartPosition} to {Input.mousePosition}");
                _dragStartPosition = Input.mousePosition;
            }
            _mousePosition = Input.mousePosition;
            _dragPath = Input.mousePosition - _dragStartPosition;
            
            if (Mathf.Abs(_dragPath.y) <= minScaleThreshold)
            {
                return;
            }
            var cachedLocalScale = transform.localScale;
            UpdateScale(_dragPath.y * scalingFactor * Time.deltaTime);
            RestorePosition(transform, cachedLocalScale);
        }

        private static bool IsDirectionChanged(Vector3 dragPath, Vector3 newDragPath)
        {
            return Vector3.Dot(
                Vector3.Project(dragPath, Vector3.up).normalized,
                Vector3.Project(newDragPath, Vector3.up).normalized
            ) < 0;
        }

        private void UpdateScale(float scale)
        {
            var localScale = transform.localScale;
            localScale.x = Mathf.Clamp(localScale.x - scale, minHeight, maxHeight);
            localScale.y = Mathf.Clamp(localScale.y + scale, minHeight, maxHeight);
            if (transform.localScale == localScale)
            {
                return;
            }
            transform.localScale = localScale;
            // Debug.Log($"Update scale: {scale}");
        }

        private static void RestorePosition(Transform cachedTransform, Vector3 cachedLocalScale)
        {
            cachedTransform.position -= Vector3.up * (cachedLocalScale.y - cachedTransform.localScale.y) / 2;
        }
        
        protected override void OnDisable()
        {
            base.OnDisable();
            projection.SetActive(false);
        }
    }
}