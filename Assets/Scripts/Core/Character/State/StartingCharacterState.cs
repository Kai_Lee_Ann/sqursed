﻿using UnityEngine;

namespace Core.Character.State
{
    public class StartingCharacterState : AbstractCharacterState
    {
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                NextState = CharacterStates.MoveForwardCharacterState;
            }
        }
    }
}