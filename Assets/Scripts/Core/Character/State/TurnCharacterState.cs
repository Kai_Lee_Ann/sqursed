﻿using UnityEngine;

namespace Core.Character.State
{
    public class TurnCharacterState : AbstractCharacterState
    {
        [SerializeField] private ScaleCharacterState scaleCharacterState;
        [SerializeField] private float movementSpeed;
        [SerializeField] private float rotationSpeed;

        private void OnEnable()
        {
            scaleCharacterState.enabled = true;
        }

        // private void OnTriggerExit(Collider other)
        // {
        //     if (other.CompareTag(Tags.TurnZone))
        //     {
        //     }
        // }

        private void Update()
        {
            transform.Translate(movementSpeed * Time.deltaTime * Vector3.forward);
            transform.rotation = Quaternion.RotateTowards(
                transform.rotation,
                Quaternion.Euler(0, -90, 0),
                Time.deltaTime * rotationSpeed
            );
            if (transform.rotation == Quaternion.Euler(0, -90, 0))
            {
                NextState = CharacterStates.MoveForwardCharacterState;
            }

            // transform.Rotate(new Vector3(0, rotationAngle * Time.deltaTime, 0));
        }

        protected override void OnDisable()
        {
            scaleCharacterState.enabled = false;
        }
    }
}