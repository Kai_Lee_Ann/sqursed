﻿using System;
using UnityEngine;

namespace Core.Character.State
{
    public abstract class AbstractCharacterState : MonoBehaviour
    {
        public Type NextState { get; protected set; }

        protected virtual void OnDisable()
        {
            // Debug.Log("OnDisable()");
            NextState = null;
        }
    }
}