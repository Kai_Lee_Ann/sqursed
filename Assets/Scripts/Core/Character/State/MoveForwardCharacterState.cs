﻿using UnityEngine;

namespace Core.Character.State
{
    public class MoveForwardCharacterState : AbstractCharacterState
    {
        [SerializeField] private float initialMovementSpeed;
        [SerializeField] private AbstractCharacterState scaleCharacterState;

        private void OnEnable()
        {
            scaleCharacterState.enabled = true;
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag(Tags.Obstacle))
            {
                NextState = CharacterStates.RecoilCharacterState;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag(Tags.FinishZone))
            {
                NextState = CharacterStates.FinishCharacterState;
                return;
            }

            if (other.gameObject.CompareTag(Tags.TurnZone))
            {
                NextState = CharacterStates.TurnCharacterState;
            }
        }

        public void Update()
        {
            transform.Translate(initialMovementSpeed * Time.deltaTime * Vector3.forward);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            scaleCharacterState.enabled = false;
        }
    }
}