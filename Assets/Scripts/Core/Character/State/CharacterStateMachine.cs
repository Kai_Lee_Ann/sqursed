﻿using System;
using UnityEngine;

namespace Core.Character.State
{
    [RequireComponent(typeof(CharacterStateFactory))]
    public class CharacterStateMachine : MonoBehaviour
    {
        private AbstractCharacterState _state;
        private CharacterStateFactory _characterStateFactory;

        private void Awake()
        {
            _characterStateFactory = GetComponent<CharacterStateFactory>();
        }

        private void Start()
        {
            GoToState(CharacterStates.StartingCharacterState);
        }

        private void GoToState(Type nextState)
        {
            if (Debug.isDebugBuild && _state != null)
            {
                Debug.Log($"Transit from state {_state.GetType().Name} into {nextState.Name}");
            }
            _state = _characterStateFactory.GetState(nextState);
            _state.enabled = true;
        }

        private void LateUpdate() 
        {
            var nextState = _state.NextState;
            if (nextState == null)
            {
                return;
            }
            _state.enabled = false;
            GoToState(nextState);
        }
    }
}