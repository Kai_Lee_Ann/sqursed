﻿using System;
using System.Collections;
using System.Collections.Specialized;
using ExtensionMethods;
using UnityEngine;

namespace Core.Character.State
{
    public class CharacterStateFactory : MonoBehaviour
    {
        private IDictionary _states;

        private void Awake()
        {
            _states = new ListDictionary();
            foreach (var state in transform.GetComponentsInChildrenOfType<AbstractCharacterState>())
            {
                state.enabled = false;
                _states.Add(state.GetType(), state);
            }
        }

        public AbstractCharacterState GetState(Type type)
        {
            var state = _states[type];
            if (state == null)
            {
                throw new ArgumentException($"State is not found: {type}");
            }
            return state as AbstractCharacterState;
        }
    }
}