﻿using UnityEngine;

namespace Core.Character
{
    public class CameraFollowing : MonoBehaviour
    {
        [SerializeField] private Transform target;

        private Vector3 _offset;
        private Quaternion _offsetRotation;

        private void Start()
        {
            _offset = target.transform.position - transform.position;
            _offsetRotation = target.transform.rotation * Quaternion.Inverse(transform.rotation);
        }

        private void LateUpdate()
        {
            transform.position = target.transform.position - _offset;
            _offsetRotation = target.transform.rotation * Quaternion.Inverse(_offsetRotation);
        }
    }
}