﻿using UnityEngine;

namespace Core.Character
{
    [RequireComponent(typeof(Renderer))]
    public class ProjectionScaler : MonoBehaviour
    {
        [SerializeField] private LayerMask projectionPlaneLayerMask;

        private Renderer _renderer;

        private void Awake()
        {
            _renderer = GetComponent<Renderer>();
        }

        private void FixedUpdate()
        {
            if (!RaycastHitObstacle(out var hit))
            {
                _renderer.enabled = false;
                return;
            }
            _renderer.enabled = true;

            var cachedTransform = transform;
            var cachedLocalScale = cachedTransform.localScale;
            UpdateScale(hit.distance, cachedTransform, cachedLocalScale);
            RestorePosition(cachedTransform, cachedLocalScale);
        }

        private static void UpdateScale(float hitDistance, Transform cachedTransform, Vector3 rescale)
        {
            rescale.z = hitDistance * rescale.z / cachedTransform.localScale.z;
            cachedTransform.localScale = rescale;
        }

        private static void RestorePosition(Transform cachedTransform, Vector3 cachedLocalScale)
        {
            cachedTransform.position -= Vector3.forward * (cachedLocalScale.z - cachedTransform.localScale.z) / 2;
        }

        private bool RaycastHitObstacle(out RaycastHit hit)
        {
            var cachedTransform = transform;
            var hitObstacle = Physics.Raycast(
                cachedTransform.position - Vector3.forward * cachedTransform.localScale.z / 2,
                cachedTransform.TransformDirection(Vector3.forward),
                out hit,
                Mathf.Infinity,
                projectionPlaneLayerMask
            );
            // if (hitObstacle)
            // {
            //     Debug.DrawRay(
            //         transform.position,
            //         transform.TransformDirection(Vector3.forward) * hit.distance,
            //         Color.yellow
            //     );
            //     Debug.Log("Did Hit");
            // }
            
            return hitObstacle;
        }
    }
}